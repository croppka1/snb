//
//  VisualisationView.swift
//  SNB
//
//  Created by Eryk Sajur on 31/05/15.
//  Copyright (c) 2015 eryk. All rights reserved.
//

import Cocoa

class VisualisationView: NSView {
    
    var outs: [[Double]] = [[Double]](count:100, repeatedValue:([Double](count:100, repeatedValue:0.0)))
    
    
    func drawArray(array: [[Double]]){
        self.outs = array;
        self.setNeedsDisplayInRect(self.frame);
    }
    
    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)
        let size: Int = self.outs.count
        let floatSize: CGFloat = CGFloat(size)
        let boundsSize: CGFloat = min(self.bounds.size.width,self.bounds.size.height);
        NSColor.blackColor().set()
        println(self.bounds)
        NSRectFill(self.frame)
        
        for i in 0..<size{
            for j in 0..<size{
                let color: NSColor = NSColor(calibratedWhite: CGFloat(abs(outs[i][j])), alpha: CGFloat(1.0))
                color.setFill()
//                println("x: \(CGFloat(i) * boundsSize/floatSize) y: \(CGFloat(j) * boundsSize/floatSize)")
                var rect: NSRect = NSRect(x: CGFloat(i) * boundsSize/floatSize , y: CGFloat(j) * boundsSize/floatSize, width: boundsSize/floatSize, height: boundsSize/floatSize)
                NSRectFill(rect)
            }
        }
        
        
        
        // Drawing code here.
    }
    
}
