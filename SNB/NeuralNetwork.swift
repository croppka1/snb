//
//  NeuralNetwork.swift
//  SNB
//
//  Created by Eryk Sajur on 30/05/15.
//  Copyright (c) 2015 eryk. All rights reserved.
//

import Cocoa
//import VisualisationView

infix operator ^^ { }
func ^^ (radix: Double, power: Double) -> Double {
    return Double(pow(Double(radix), Double(power)))
}

class NeuralNetwork: NSObject {
    var didFinish: Bool = false;
    
    
    var view: VisualisationView?
    
    let numberOfInputs: Int = Int(2)
    let numberOfHiddenNeurons:Int = Int(7)
    let numberOfOutputs: Int = Int(1)
    
    var expectedOutput: Double = Double(0)
    var globalI: Int = Int(0)
    var reps: Int = Int(0)
   
    var iterations: Int = Int(0)
    var bestIteration: Int = Int(0)
    var bestNetError: Double = Double(100000);
    
    var netError: Double = Double(0.0)
   
    let beta: Double = Double(1.0)
    let bias: Double = Double(0.0)
    
    var outError: Double = Double(0)
    let learningRate: Double = Double(0.3)
    let IHBias: Double = Double(2.0)
//    let HJBias: Double = Double(10.0)
    let ILBias:Double = Double(1.0)
    let HLBias:Double = Double(1.0)
    var biasError:Double = Double(0.0)
    
    var WILBias:[[Double]] //
    var WHLBias: [[Double]]
    
    var hidError: [Double]
    var ILIns: [[Double]]
    var Wih: [[Double]]
    var HLState: [[Double]]
    var HLOuts: [[Double]]
    var Whj: [[Double]]
    var OLState: [[Double]]
    var OLOuts: [[Double]]
    
    var data: [[Double]] = [[Double]]()
    var learningData: [[Double]]!
    var testData: [[Double]]!
    
    override init() {
        WILBias = [[Double]](count:1, repeatedValue:([Double](count:numberOfHiddenNeurons, repeatedValue:0.0)))
        WHLBias = [[Double]](count:1, repeatedValue:([Double](count:1, repeatedValue:0.0)))
        hidError = [Double](count: numberOfHiddenNeurons, repeatedValue:0.0);
        ILIns = [[Double]](count:1, repeatedValue:([Double](count:numberOfInputs, repeatedValue:0.0)))
        Wih = [[Double]](count:numberOfInputs, repeatedValue:([Double](count:numberOfHiddenNeurons, repeatedValue:0.0)))
        HLState = [[Double]](count:1, repeatedValue:([Double](count:numberOfHiddenNeurons, repeatedValue:0.0)))
        HLOuts  = [[Double]](count:1, repeatedValue:([Double](count:numberOfHiddenNeurons, repeatedValue:0.0)))
        Whj = [[Double]](count:numberOfHiddenNeurons, repeatedValue:([Double](count:numberOfOutputs, repeatedValue:0.0)))
        OLState = [[Double]](count:1, repeatedValue:([Double](count:1, repeatedValue:0.0)))
        OLOuts  = [[Double]](count:1, repeatedValue:([Double](count:1, repeatedValue:0.0)))
        
        let path = NSBundle.mainBundle().pathForResource("dane", ofType: "txt")
        let test = min(0.0, Double.NaN);
        
        if let content = String(contentsOfFile:path!, encoding: NSUTF8StringEncoding, error: nil) {
            var array = content.componentsSeparatedByString("\n")
            for string in array {
                var contents = string.componentsSeparatedByString(";")
                var doubleArr: [Double] = [Double]()
                for str in contents {
                    doubleArr.append((str as NSString).doubleValue)
                }
                data.append(doubleArr)
            }
            learningData = [[Double]](data[0..<90000])
            testData = [[Double]](data[90000..<data.count])
            
        }
        
        super.init()
        
        self.initializeWeights(&Wih)
        self.initializeWeights(&Whj)
    }
    
    func calcNet(){
        self.didFinish = false;
        
        while true {
            self.getILIns()
            self.propagateForward()
            
            self.OLError()
            self.HLError()
            
            self.changeHLWeights()
            self.changeOLWeights()
            
            
        }
    }
    
    private func initializeWeights(inout weightArray: [[Double]]){
        for i in 0..<weightArray.count {
            for j in 0..<weightArray[0].count{
                let randomValue: Double = Double(0.2) * Double(arc4random())/Double(UINT32_MAX) + Double(-0.1);
//                println(randomValue)
                weightArray[i][j] = randomValue
            }
        }
    
    }
    private func getILIns() {
        var temp: [Double] = learningData[globalI]
        
        globalI++
        
        ILIns[0][0] = temp[0]
        ILIns[0][1] = temp[1]
        expectedOutput = Double(temp[2])
        
//        println("\(globalI), \(iterations)")
        if globalI == 899 {
//            println("\(globalI), \(iterations)")
//            if iterations%100 == 0 {
//                self.calcNetError()
//                println(netError);
//            }
            
            
            
            globalI = 0
            iterations++
            if(iterations%100 == 0){
                println(iterations);
                self.calcNetError();
                self.drawProgress()
            }
            
            didFinish = true
            learningData = sorted(learningData){_, _ in arc4random() % 2 == 0}
        }
    }

    
    
    func propagateForward() {
        
        HLState = [[Double]](count:1, repeatedValue:([Double](count:numberOfHiddenNeurons, repeatedValue:0.0)))
        OLState = [[Double]](count:1, repeatedValue:([Double](count:1, repeatedValue:0.0)))
        
        calcLayerState(ILIns, w: Wih, state: &HLState, bias: ILBias, biasWeight: WILBias)
        calcLayerOuts(&HLOuts, state: HLState)
        calcLayerState(HLOuts, w: Whj, state: &OLState, bias: HLBias, biasWeight: WHLBias)
        calcLayerOuts(&OLOuts, state: OLState)
        
//        println(OLOuts);
        
    }
    
    func calcLayerState(ins: [[Double]], w: [[Double]], inout state: [[Double]], bias: Double, biasWeight: [[Double]]) {
        for i in 0..<state[0].count {
            for j in 0..<ins[0].count {
                state[0][i] += ins[0][j] * w[j][i]
            }
            state[0][i] += bias * biasWeight[0][i]
        }
    }
    
    func calcLayerOuts(inout outs: [[Double]], state: [[Double]]){
        for i in 0 ..< outs[0].count {
            outs[0][i] = function(state[0][i])
        }
    }
    
    private func OLError() {
        outError = (expectedOutput - OLOuts[0][0]) * derivative(OLState[0][0]);
    }

    private func HLError() {
        for i in 0..<hidError.count {
            hidError[i] = derivative(HLState[0][i]) * outError * Whj[i][0]
        }
        biasError = derivative(1) * outError * 1;
    }
    
    private func changeOLWeights() {
        for i in 0..<Whj.count {
            Whj[i][0] += learningRate * outError * HLOuts[0][i]
        }
        WHLBias[0][0] += learningRate * outError * 1;
    }
    
    private func changeHLWeights() {
        for i in 0..<Wih[0].count {
            for j in  0..<Wih.count {
                Wih[j][i] += learningRate * hidError[i] * ILIns[0][j];
            }
            WILBias[0][i] += learningRate * biasError * 1.0;
        }
    }
    
    private func calcNetError() {
        netError = 0;
        for i in 0..<testData.count{
            let temp: [Double] = testData[i]

            ILIns[0][0] = temp[0]
            ILIns[0][1] = temp[1]
            expectedOutput = temp[2]
            
            propagateForward()
            
            netError += abs(expectedOutput - OLOuts[0][0]);
        }
        netError = (netError)/(Double(testData.count))*100.0;
        if(netError < bestNetError){
            bestNetError = netError;
            bestIteration = iterations;
        }
        testData = sorted(testData){_, _ in arc4random() % 2 == 0}
    }
    
    
    
    func function(x: Double) -> Double {

        (1.0 + exp(-(beta * x)));
        var expression: Double = (exp(-(beta * x)))
//        expression = expression.isNaN ? 0.0 : expression;
        return 1/(1.0 + expression);
    }
    
    
    func derivative(x: Double) -> Double{
        let top: Double = beta * exp(beta * (x + bias))
        let bottom: Double  = pow((beta * (x + bias) + 1.0), Double(2))
//        println("derivative : top = \(top), bottom = \(bottom)")
//        return top / bottom //(beta * exp(beta * (x + bias))) / ((beta * (x + bias) + 1.0)^^(Int(2)))
        return (beta * exp(beta * (x + bias))) / pow(exp(beta * ( x + bias)) + 1.0, 2)
    }
    
    
    private func drawProgress() {
        var temp: [[Double]] = [[Double]](count:100, repeatedValue:([Double](count:100, repeatedValue:0.0)))
        for i in 0..<100 {
            ILIns[0][0] = Double(i) * 0.01
            for j in 0..<100 {
                ILIns[0][1] = 1 - Double(j) * 0.01;
                propagateForward();
                temp[i][j] = OLOuts[0][0]
            }
        }
        dispatch_async(dispatch_get_main_queue()) {
            self.view!.drawArray(temp);
        }
        
    }
    
}
