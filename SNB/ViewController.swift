//
//  ViewController.swift
//  SNB
//
//  Created by Eryk Sajur on 30/05/15.
//  Copyright (c) 2015 eryk. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    
    @IBOutlet weak var visView: VisualisationView!
    let network: NeuralNetwork = NeuralNetwork()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        network.view = visView;
        
        // Do any additional setup after loading the view.
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
//    override func viewDidAppear() {
////        network.calcNet()
//    }


    @IBAction func buttonPressed(sender: NSButton) {
        sender.state = 2;
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            self.network.calcNet();
        }
    }
}

